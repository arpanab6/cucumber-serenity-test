
 Feature: Access to Pet store orders

	Scenario: place an order for a pet
 		Given the request with order details
 		When the request is made with the post method to place order 
		Then the order placed successfully is returned
		
	Scenario: get an order for a pet
 		Given the request with order Id 
 		When the request is made with the Get method to retrieve an order 
		Then the order get successfully is returned
		
	Scenario: retrieve store inventory
 		Given the request to retrieve inventory  
 		When the request is made with the Get method to retrieve inventory 
		Then the inventory get successfully is returned
	
	Scenario: delete an order by orderId
 		Given the request to delete an order with Id 
 		When the request is made with the delete method
		Then the order deleted successfully is returned