 Feature: Operations about user

	Scenario: Creates list of users with given input array
 		Given the request with list of users
 		When the request is made with the post method to create with array list of users
		Then the users created successfully is returned
		
	Scenario: Creates list of users with given input array
 		Given the request with list of users
 		When the request is made with the post method to create with list of users
		Then the users created successfully is returned
		
	Scenario: Logs user into the system
 		Given the request with user name "usertest1" and password "password@1"
 		When the request is made with the get method to login
		Then the user get successfully logged is returned
		
		
	Scenario: Get user by user name
 		Given the request with user name "usertest1"
 		When the request is made with the get method to get a user record
		Then the user get successfully is returned
		
	Scenario: Get user by invalid user name
 		Given the request with invalid user name "usertest123"
 		When the request is made with the get method to get invalid user record
		Then the user not found is returned
		
	Scenario: Delete user by user name
 		Given the request to delete with user name "usertest1"
 		When the request is made with the delete method to delete a user record
		Then the user deleted successfully is returned
		
	Scenario: Delete user by invalid user name
 		Given the request to delete with invalid user name "usertest12345"
 		When the request is made with the delete method to delete invalid user record
		Then the user to be deleted not found is returned
		
	Scenario: Update user by user name
 		Given the request with user name "usertest2" and user record
 		When the request is made with the put method to update a user record
		Then the user updated successfully is returned
		
	Scenario: Create user
 		Given the request for creating user with user details
 		When the request is made with the post method to create user
		Then the user created successfully is returned
		
	Scenario: Logout user
 		Given the request for Logout with the sessionId
 		When the request is made with the get method to logout user
		Then the user logout successfully is returned
		
	