package com.cucumber.petstore.cucumberSerenitytest.models;

public enum Status {
	Available("available"),
	Pending("pending"),
	Sold("sold");
	
	private final String status;

	Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

}
