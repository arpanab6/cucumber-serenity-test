package com.cucumber.petstore.cucumberSerenitytest.models;

public class Order {
	
	private int id;
	private int petId;
	private int quantity;
	private String shipDate;
	private OrderStatus orderStatus;
	private boolean complete;
	public Order(int id, int petId, int quantity, String shipDate, OrderStatus orderStatus, boolean complete) {
		this.id = id;
		this.petId = petId;
		this.quantity = quantity;
		this.shipDate = shipDate;
		this.orderStatus = orderStatus;
		this.complete = complete;
	}
	public int getId() {
		return id;
	}
	public void setId(int orderId) {
		this.id = id;
	}
	public int getPetId() {
		return petId;
	}
	public void setPetId(int petId) {
		this.petId = petId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getShipDate() {
		return shipDate;
	}
	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}
	public OrderStatus getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	public boolean isComplete() {
		return complete;
	}
	public void setComplete(boolean complete) {
		this.complete = complete;
	}
	
	

}
