package com.cucumber.petstore.cucumberSerenitytest.models;

public enum OrderStatus {
	
	Placed("placed"),
	Approved("approved"),
	Delivered("delivered");
	
	private final String orderStatus;

	OrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus() {
        return this.orderStatus;
    }


}
