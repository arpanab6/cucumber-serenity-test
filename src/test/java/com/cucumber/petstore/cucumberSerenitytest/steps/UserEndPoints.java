package com.cucumber.petstore.cucumberSerenitytest.steps;

public enum UserEndPoints {
    USERLISTARRAY("/user/createWithArray"),
    USERLIST("/user/createWithList"),
    GETUSER("/user/{username}"),
    UPDATEUSER("/user/{username}"),
    DELETEUSER("/user/{username}"),
    LOGINUSER("/user/login"),
    LOGOUTUSER("/user/logout"),
	CREATEUSER("/user");


    private final String url;

    UserEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}