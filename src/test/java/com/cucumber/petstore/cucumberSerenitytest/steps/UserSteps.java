package com.cucumber.petstore.cucumberSerenitytest.steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import static org.hamcrest.Matchers.*;

import org.apache.http.HttpStatus;

import com.cucumber.petstore.cucumberSerenitytest.models.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.Gson;
import net.serenitybdd.rest.SerenityRest;

public class UserSteps  extends BaseTest {
	
	User[] users = new User[] {new User(0,"usertest1","user1","last1","email1@gmail.com","Test@123","+31647399023",0),
							   new User(0,"usertest2","user2","last2","email2@gmail.com","Test@1234","+31647399024",0),
							   new User(0,"usertest3","user3","last3","email3@gmail.com","Test@12345","+31647399025",0)};
	
	 protected static String sessionId="";
	
	 //Create a list of users
    @Given("the request with list of users")
    public void theRequestWithListOfUsers() {
    	
    	Gson gsone = new Gson();
    	String json = gsone.toJson(users);
       
		 
        SerenityRest.given().contentType("application/json").accept("application/json").body(json).baseUri(BASE_URI);
                
    }
	
	 @When("the request is made with the post method to create with array list of users")
	    public void theRequestIsMadeWithThePostMethodToCreateWithArrayListOfUsers() {
		SerenityRest.when().post(UserEndPoints.USERLISTARRAY.getUrl());
		
	    }
	 
	 @When("the request is made with the post method to create with list of users")
	    public void theRequestIsMadeWithThePostMethodToCreateWithListOfUsers() {
		SerenityRest.when().post(UserEndPoints.USERLIST.getUrl());
		
	    }

	 @Then("the users created successfully is returned")
	    public void theUsersCreatedSuccessfullyIsReturned() {
	        restAssuredThat(response -> response
	                .statusCode(HttpStatus.SC_OK));
	       
	        	        
	    }
	 
	 //User login 
	 @Given("the request with user name {string} and password {string}")
	    public void theRequestWithUserNameAndPassword(String userName, String password) {
	    	 
	        SerenityRest.given().accept("application/json").baseUri(BASE_URI).queryParam("username", userName).queryParam("password",password);
	                
	    }
		
	@When("the request is made with the get method to login")
		    public void theRequestIsMadeWithTheGetMethodToLogin() {
			SerenityRest.when().get(UserEndPoints.LOGINUSER.getUrl());
			
		    }
	
	@Then("the user get successfully logged is returned")
    public void theUserGetSuccessfullyLoggedIsReturned() {
        restAssuredThat(response -> response
                .statusCode(HttpStatus.SC_OK));
        
        sessionId = SerenityRest.lastResponse().getBody().jsonPath().getString("message").split(":")[1];
       
    }
	 
	//get a user with user name
	    @Given("the request with user name {string}")
	    public void theRequestWithUserName(String userName) {
	    	 
	        SerenityRest.given().accept("application/json").baseUri(BASE_URI).pathParam("username", userName);
	                
	    }
		
		 @When("the request is made with the get method to get a user record")
		    public void theRequestIsMadeWithTheGetMethodToGetUserRecord() {
			SerenityRest.when().get(UserEndPoints.GETUSER.getUrl());
			
		    }
		 
		 @Then("the user get successfully is returned")
		    public void theUserGetSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK));
 	        
		    }
		 
		//get a user with invalid user name
		    @Given("the request with invalid user name {string}")
		    public void theRequestWithInvalidUserName(String userName) {
		    	 
		        SerenityRest.given().accept("application/json").baseUri(BASE_URI).pathParam("username", userName);
		                
		    }
			
			 @When("the request is made with the get method to get invalid user record")
			    public void theRequestIsMadeWithTheGetMethodToGetInvalidUserRecord() {
				SerenityRest.when().get(UserEndPoints.GETUSER.getUrl());
				
			    }
			 
			 @Then("the user not found is returned")
			    public void theUserNotFoundIsReturned() {
			        restAssuredThat(response -> response
			                .statusCode(HttpStatus.SC_NOT_FOUND).body("message",equalTo("User not found")));
	 	        
			    }
		 
		 //delete a user with user name
		 
		 @Given("the request to delete with user name {string}")
		    public void theRequestTpDeleteWithUserName(String userName) {
		    	 
		        SerenityRest.given().accept("application/json").baseUri(BASE_URI).pathParam("username", userName).header("sessionId",sessionId );
		                
		    }
		 
		 @When("the request is made with the delete method to delete a user record")
		    public void theRequestIsMadeWithTheDeleteMethodToDeleteUserRecord() {
			SerenityRest.when().delete(UserEndPoints.DELETEUSER.getUrl());
			
		    }

		 		 
		 @Then("the user deleted successfully is returned")
		    public void theUserDeletedSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK));
 	        
		    }
		 
		 
//delete a user with invalid user name
		 
		 @Given("the request to delete with invalid user name {string}")
		    public void theRequestTpDeleteWithInvalidUserName(String userName) {
		    	 
		        SerenityRest.given().accept("application/json").baseUri(BASE_URI).pathParam("username", userName).header("sessionId",sessionId );
		                
		    }
		 
		 @When("the request is made with the delete method to delete invalid user record")
		    public void theRequestIsMadeWithTheDeleteMethodToDeleteInvalidUserRecord() {
			SerenityRest.when().delete(UserEndPoints.DELETEUSER.getUrl());
			
		    }

		 		 
		 @Then("the user to be deleted not found is returned")
		    public void theUserToBeDeletedNotFoundIsReturned() {
		        restAssuredThat(response -> response
		        		.statusCode(HttpStatus.SC_NOT_FOUND));
 	        
		    }
		 
		//update a user using user name
		    @Given("the request with user name {string} and user record")
		    public void theRequestWithUserNameAndUserRecord(String userName) {
		    	
		    	Map<String, Object> jsonAsMap = new HashMap<>();
		        jsonAsMap.put("firstname", "John");
		        jsonAsMap.put("lastname", "oliver");
		        jsonAsMap.put("email", "email90@gmail.com");
		        jsonAsMap.put("phone", "+31067896543");
		        
		    	 
		        SerenityRest.given().accept("application/json").contentType("application/json").baseUri(BASE_URI).pathParam("username", userName).body(jsonAsMap).header("sessionId",sessionId );
		                
		    }
		    
		    @When("the request is made with the put method to update a user record")
		    public void theRequestIsMadeWithThePutMethodToUpdateUserRecord() {
			SerenityRest.when().put(UserEndPoints.UPDATEUSER.getUrl());
			
		    }

		 		 
		 @Then("the user updated successfully is returned")
		    public void theUserUpdatedSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK));
	        
		    }
		 
		 
		
				 
		//Create a user
		    @Given("the request for creating user with user details")
		    public void theRequestForCreatingUserWithUserDetails() {
		    	
		    	int id = Math.abs(new Random().nextInt());
		        Map<String, Object> jsonAsMap = new HashMap<>();
		        jsonAsMap.put("id", id);
		    	jsonAsMap.put("username", "oliverjohn");
		        jsonAsMap.put("firstname", "John");
		        jsonAsMap.put("lastname", "oliver");
		        jsonAsMap.put("email", "email90@gmail.com");
		        jsonAsMap.put("phone", "+31067896543");
		        jsonAsMap.put("password", "password$1");
		        jsonAsMap.put("userstatus", 0);
		        
		        
		    	 
		        SerenityRest.given().accept("application/json").contentType("application/json").baseUri(BASE_URI).body(jsonAsMap).header("sessionId",sessionId );
		                
		    }
		    
		    @When("the request is made with the post method to create user")
		    public void theRequestIsMadeWithThePostMethodToCreateUser() {
			SerenityRest.when().post(UserEndPoints.CREATEUSER.getUrl());
			
		    }

		 		 
		 @Then("the user created successfully is returned")
		    public void theUserCreatedSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK));
	        
		    }
		 
		//Logout a user with sessionId
		    @Given("the request for Logout with the sessionId")
		    public void theRequestForLogoutrWithTheSessionId() {
		    	
		    	    	 
		        SerenityRest.given().accept("application/json").baseUri(BASE_URI).header("sessionId",sessionId );
		                
		    }
		    
		    @When("the request is made with the get method to logout user")
		    public void theRequestIsMadeWithThegettMethodToLogoutUser() {
			SerenityRest.when().get(UserEndPoints.LOGOUTUSER.getUrl());
			
		    }

		 		 
		 @Then("the user logout successfully is returned")
		    public void theUserLogoutSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK));
	        
		    }
	 
	 
}