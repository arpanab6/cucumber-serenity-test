package com.cucumber.petstore.cucumberSerenitytest.steps;

public enum PetEndpoints {
    UPLOADIMAGE("/pet/{petId}/uploadImage"),
    ADDNEWPET("/pet"),
    UPDATEPET("/pet"),
    GETPETSTATUS("/pet/findByStatus"),
    GETPET("/pet/{petId}"),
    UPDATEPETBYID("/pet/{petId}"),
    DELETEPET("/pet/{petId}");


    private final String url;

    PetEndpoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}