package com.cucumber.petstore.cucumberSerenitytest.steps;

public enum StoreEndpoints {
    PLACEORDER("/store/order"),
    GETORDER("/store/order/{orderId}"),
    DELETEORDER("/store/order/{orderId}"),
    GETINVENTORY("/store/inventory");
    


    private final String url;

    StoreEndpoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}