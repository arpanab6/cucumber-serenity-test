package com.cucumber.petstore.cucumberSerenitytest.steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpStatus;

import com.cucumber.petstore.cucumberSerenitytest.models.*;

import static org.hamcrest.Matchers.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;

public class StoreSteps  extends BaseTest {
	
	protected Order order = new Order(1,1,10,"2020-11-16T01:09:21.567Z",OrderStatus.Placed,false);
	protected Order invalidOrder = new Order(-1,-1,-1,"2020-11-16T01:09:21.567Z",OrderStatus.Delivered,false);
	final int invalidOrderId =-1;
	
	 //Place a new order
    @Given("the request with order details")
    public void theRequestWithOrderDetails() {
    	
    	Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("id", order.getId());
        jsonAsMap.put("petId", order.getPetId());
        jsonAsMap.put("quantity", order.getQuantity());
        jsonAsMap.put("shipDate", order.getShipDate());
        jsonAsMap.put("status", order.getOrderStatus());
        jsonAsMap.put("complete", order.isComplete());
       
		 
        SerenityRest.given().contentType("application/json").accept("application/json").body(jsonAsMap).baseUri(BASE_URI);
                
    }
	
	 @When("the request is made with the post method to place order")
	    public void theRequestIsMadeWithThePostMethodToPlaceOrder() {
		SerenityRest.when().post(StoreEndpoints.PLACEORDER.getUrl());
		
	    }

	 @Then("the order placed successfully is returned")
	    public void thePetPlacedSuccessfullyIsReturned() {
	        restAssuredThat(response -> response
	                .statusCode(HttpStatus.SC_OK).body("petId", equalTo(order.getPetId())).body("quantity", equalTo(order.getQuantity())));
	        
	        order.setId(SerenityRest.lastResponse().getBody().jsonPath().getInt("id"));
	 }
	 
	 
	 //Place an invalid order
	 //with invalid quantity and id , API is responding with status code 200
	    @Given("the request with invalid order details")
	    public void theRequestWithInvalidOrderDetails() {
	    	
	    	Map<String, Object> jsonAsMap = new HashMap<>();
	        jsonAsMap.put("id", invalidOrder.getId());
	        jsonAsMap.put("petId", invalidOrder.getPetId());
	        jsonAsMap.put("quantity", invalidOrder.getQuantity());
	        jsonAsMap.put("shipDate", invalidOrder.getShipDate());
	        jsonAsMap.put("status", invalidOrder.getOrderStatus());
	        jsonAsMap.put("complete", invalidOrder.isComplete());
	       
			 
	        SerenityRest.given().contentType("application/json").accept("application/json").body(jsonAsMap).baseUri(BASE_URI);
	                
	    }
		
		 @When("the request is made with the post method to place invalid order")
		    public void theRequestIsMadeWithThePostMethodToPlaceInvalidOrder() {
			SerenityRest.when().post(StoreEndpoints.PLACEORDER.getUrl());
			
		    }

		 @Then("the invalid order placed successfully is returned")
		    public void theInvalidPlacedSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK).body("petId", equalTo(-1)).body("quantity", equalTo(-1)));
		        
		       }
	 
	//find order by Id
	 @Given("the request with order Id")
	    public void theRequestWithOrderId() {
			 
	        SerenityRest.given().accept("application/json").baseUri(BASE_URI)
	            .pathParam("orderId", order.getId());
	                
	    }
		
		 @When("the request is made with the Get method to retrieve an order")
		    public void theRequestIsMadeWithTheGetMethodToRetrieveAnOrder() {
			SerenityRest.when().get(StoreEndpoints.GETORDER.getUrl());
			
		    }

		    @Then("the order get successfully is returned")
		    public void theOrderGetSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK).body("id", equalTo(order.getId())));
		        
		    }
	
		    
		  //find order by Id not exist
			 @Given("the request with order Id not exist")
			    public void theRequestWithOrderIdNotExist() {
					 
			        SerenityRest.given().accept("application/json").baseUri(BASE_URI)
			            .pathParam("orderId", invalidOrderId);
			                
			    }
				
				 @When("the request is made with the Get method to retrieve an order not exist")
				    public void theRequestIsMadeWithTheGetMethodToRetrieveAnOrderNotExist() {
					SerenityRest.when().get(StoreEndpoints.GETORDER.getUrl());
					
				    }

				    @Then("the order not found is returned")
				    public void theOrderNotFoundIsReturned() {
				        restAssuredThat(response -> response
				                .statusCode(HttpStatus.SC_NOT_FOUND).body("message", equalTo("Order not found")));
				        
				    }
	 
	//retrieve inventory
	 @Given("the request to retrieve inventory")
	    public void theRequestToRetrieveInventory() {
			 
	        SerenityRest.given().accept("application/json").baseUri(BASE_URI);
	                       
	    }
		
		 @When("the request is made with the Get method to retrieve inventory")
		    public void theRequestIsMadeWithTheGetMethodToRetrieveInventory() {
			SerenityRest.when().get(StoreEndpoints.GETINVENTORY.getUrl());
			
		    }

		    @Then("the inventory get successfully is returned")
		    public void theInventoryGetSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK));
		        
		    }
		    
		  //delete an order by Id
			 @Given("the request to delete an order with Id")
			    public void theRequestToDeleteAnOrderWithId() {
					 
			        SerenityRest.given().accept("application/json").baseUri(BASE_URI).baseUri(BASE_URI)
		            .pathParam("orderId", order.getId());;
			                       
			    }
				
			 @When("the request is made with the delete method")
				    public void theRequestIsMadeWithTheDeleteMethod() {
					SerenityRest.when().delete(StoreEndpoints.DELETEORDER.getUrl());
					
				    }

			 @Then("the order deleted successfully is returned")
				    public void theOrderDeletedSuccessfullyIsReturned() {
				        restAssuredThat(response -> response
				                .statusCode(HttpStatus.SC_OK));
				        
				    }
			 
			//delete an order does not exist by Id
			 @Given("the request to delete an order with Id not exist")
			    public void theRequestToDeleteAnOrderWithIdNotExist() {
					 
			        SerenityRest.given().accept("application/json").baseUri(BASE_URI).baseUri(BASE_URI)
		            .pathParam("orderId", invalidOrderId);;
			                       
			    }
				
			 @When("the request is made with the delete method for order not exist")
				    public void theRequestIsMadeWithTheDeleteMethodForOrderNotExist() {
					SerenityRest.when().delete(StoreEndpoints.DELETEORDER.getUrl());
					
				    }

			 @Then("the order does not exist is returned")
				    public void theOrderDoesNotExistIsReturned() {
				        restAssuredThat(response -> response
				                .statusCode(HttpStatus.SC_NOT_FOUND).body("message", equalTo("Order Not Found")));
				        
				    }
			

}
