package com.cucumber.petstore.cucumberSerenitytest.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;

import com.cucumber.petstore.cucumberSerenitytest.models.*;
import com.gargoylesoftware.htmlunit.javascript.host.fetch.Response;

import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class PetSteps extends BaseTest { 
	
	final int invalidPetId=123321;
	protected Category category = new Category(1,"Herding");
	protected Tag tag = new Tag(1,"German Shepard");
	protected Status status = Status.Available;
	protected Pet pet = new Pet(1,category,"Marshall",new String[]{this.getClass().getClassLoader().getResource("Images/Doggie.jpg").toString()},tag,status);
	
	// Upload Image of pet	
	@Given("the request with pet Id {int} and valid image")
    public void theRequestWithPetIdAndValidImage(int petId) {
		 
        SerenityRest.given().contentType("multipart/form-data").accept("application/json").multiPart("file",new File (this.getClass().getClassLoader().getResource("Images/Doggie.jpg").getFile()) ,"image/jpeg").multiPart("additionalMetadata","upload doggie image").baseUri(BASE_URI)
            .pathParam("petId", petId);
                
    }
	
	 @When("the request is made with the post method to upload image")
	    public void theRequestIsMadeWithPostMethodToUploadImage() {
		SerenityRest.when().post(PetEndpoints.UPLOADIMAGE.getUrl());
		
	    }

	    @Then("the image uploaded successfully is returned")
	    public void theImageUploadedSuccessfullyIsReturned() {
	        restAssuredThat(response -> response
	                .statusCode(HttpStatus.SC_OK));
	        
	    }

	
	    //Add a new pet
	    @Given("the request with pet details")
	    public void theRequestWithPetDetails() {
	    	
	    	
            Map<String, Object> jsonAsMap = new HashMap<>();
            jsonAsMap.put("id", pet.getId());
            jsonAsMap.put("category", pet.getCategory());
            jsonAsMap.put("name", pet.getName());
            jsonAsMap.put("tag", pet.getTag());
            jsonAsMap.put("status", pet.getStatus());
            jsonAsMap.put("photoUrls", pet.getPhotoUrls());
           
			 
	        SerenityRest.given().contentType("application/json").accept("application/json").body(jsonAsMap).baseUri(BASE_URI);
	                
	    }
		
		 @When("the request is made with the post method to add new pets")
		    public void theRequestIsMadeWithThePostMethodToAddNewPets() {
			SerenityRest.when().post(PetEndpoints.ADDNEWPET.getUrl());
			
		    }

		 @Then("the pet added successfully is returned")
		    public void thePetAddedSuccessfullyIsReturned() {
		        restAssuredThat(response -> response
		                .statusCode(HttpStatus.SC_OK));
		       
		        pet.setId(SerenityRest.lastResponse().getBody().as(Pet.class).getId());
		        
		    }

		 //Update a pet details
		 
		 @Given("the request with pet details to be updated")
		    public void theRequestWithPetDetailToBeUpdated() {
		    	
		    	Map<String, Object> jsonAsMap = new HashMap<>();
	            jsonAsMap.put("id", pet.getId());
	            jsonAsMap.put("category", pet.getCategory());
	            jsonAsMap.put("name", pet.getName());
	            jsonAsMap.put("tag", pet.getTag());
	            jsonAsMap.put("status", pet.getStatus());
	            jsonAsMap.put("photoUrls", pet.getPhotoUrls());
	           
				 
		        SerenityRest.given().contentType("application/json").accept("application/json").body(jsonAsMap).baseUri(BASE_URI);
		                
		    }
			
			 @When("the request is made with the put method to update or create new pets")
			    public void theRequestIsMadeWithThePutMethodToUpdateOrCreateNewPets() {
				SerenityRest.when().post(PetEndpoints.UPDATEPET.getUrl());
				
			    }

			 @Then("the pet updated or added successfully is returned")
			    public void thePetUpdatedOrAddedSuccessfullyIsReturned() {
			        restAssuredThat(response -> response
			                .statusCode(HttpStatus.SC_OK));
			        
			    }

	//find pet by status
			 @Given("the request with {string} status")
			    public void theRequestWithStatus(String status) {
					 
			        SerenityRest.given().contentType("application/json").accept("application/json").baseUri(BASE_URI)
			            .queryParam("status", status);
			                
			    }
				
				 @When("the request is made with the get method to find pet by status")
				    public void theRequestIsMadeWithTheGetMethodToFindPetByStatus() {
					SerenityRest.when().get(PetEndpoints.GETPETSTATUS.getUrl());
					
				    }

				    @Then("the pet details successfully is returned")
				    public void thePetDetailsSuccessfullyIsReturned() {
				        restAssuredThat(response -> response
				                .statusCode(HttpStatus.SC_OK));
				        
				    }
				    
				  //find pet by Id
					 @Given("the request with Id {int}")
					    public void theRequestWithId(int petId) {
							 
					        SerenityRest.given().contentType("application/json").accept("application/json").baseUri(BASE_URI)
					            .pathParam("petId", petId);
					                
					    }
						
						 @When("the request is made with the get method to find pet by Id")
						    public void theRequestIsMadeWithTheGetMethodToFindPetById() {
							SerenityRest.when().get(PetEndpoints.GETPET.getUrl());
							
						    }

						    @Then("the pet details successfully is returned by Id")
						    public void thePetDetailsSuccessfullyIsReturnedById() {
						        restAssuredThat(response -> response
						                .statusCode(HttpStatus.SC_OK));
						        
						    }
						    
						    
						    //find pet by invalid Id
							 @Given("the request with invalid Id {int}")
							    public void theRequestWithInvalidId(int petId) {
									 
							        SerenityRest.given().contentType("application/json").accept("application/json").baseUri(BASE_URI)
							            .pathParam("petId", invalidPetId);
							                
							    }
								
								 @When("the request is made with the get method to find pet by invalid Id")
								    public void theRequestIsMadeWithTheGetMethodToFindPetByInvalidId() {
									SerenityRest.when().get(PetEndpoints.GETPET.getUrl());
									
								    }

								    @Then("the Pet not found is returned")
								    public void thePetNotFoundIsReturnedById() {
								        restAssuredThat(response -> response
								                .statusCode(HttpStatus.SC_NOT_FOUND).body("message",equalTo("Pet not found")));
								        
								    }

						  //update pet name and status by Id
							 @Given("the request with  Id {int} and status {string} and name {string}")
							    public void theRequestWithIdAndStatusAndName(int petId, String status, String name) {
									 
							        SerenityRest.given().contentType("application/x-www-form-urlencoded").accept("application/json").baseUri(BASE_URI)
							            .pathParam("petId", petId).formParam("status", status,"name",name);
							                
							    }
								
								 @When("the request is made with the post method to update pet by Id")
								    public void theRequestIsMadeWithThePostMethodToUpdatePetById() {
									SerenityRest.when().post(PetEndpoints.UPDATEPETBYID.getUrl());
									
								    }

								    @Then("the pet details successfully updated is returned")
								    public void thePetDetailsSuccessfullyUpdatedIsReturned() {
								        restAssuredThat(response -> response
								                .statusCode(HttpStatus.SC_OK));
								        
								    }
								    
								    //update pet name and status by invalid Id
									 @Given("the request with invalid Id {int} and status {string} and name {string}")
									    public void theRequestWithInvalidIdAndStatusAndName(int petId, String status, String name) {
											 
									        SerenityRest.given().contentType("application/x-www-form-urlencoded").accept("application/json").baseUri(BASE_URI)
									            .pathParam("petId", invalidPetId).formParam("status", status,"name",name);
									                
									    }
										
										 @When("the request is made with the post method to update pet by invalid Id")
										    public void theRequestIsMadeWithThePostMethodToUpdatePetByInvalidId() {
											SerenityRest.when().post(PetEndpoints.UPDATEPETBYID.getUrl());
											
										    }

										    @Then("the pet details not found is returned")
										    public void thePetDetailsNotFoundIsReturned() {
										        restAssuredThat(response -> response
										                .statusCode(HttpStatus.SC_NOT_FOUND).body("message",equalTo("not found")));
										        
										    }
								    
						//delete pet by Id
							@Given("the request to delete pet with  Id {int}")
							public void theRequestToDeletePetWithId(int petId) {
											 
							       SerenityRest.given().accept("application/json").baseUri(BASE_URI).header("api_key", "special-key")
							       .pathParam("petId", petId);
									                
									    }
										
							 @When("the request is made with the delete method to delete pet by Id")
							 public void theRequestIsMadeWithTheDeleteMethodToDeletePetById() {
							SerenityRest.when().post(PetEndpoints.DELETEPET.getUrl());
											
										    }

							 @Then("the pet details successfully deleted is returned")
							  public void thePetDetailsSuccessfullyDeletedIsReturned() {
							       restAssuredThat(response -> response
								                .statusCode(HttpStatus.SC_OK));
										        
										    }
							//delete pet by invalid Id
								@Given("the request to delete pet with invalid Id {int}")
								public void theRequestToDeletePetWithInvalidId(int petId) {
												 
								       SerenityRest.given().accept("application/json").baseUri(BASE_URI).header("api_key", "special-key")
								       .pathParam("petId", invalidPetId);
										                
										    }
											
								 @When("the request is made with the delete method to delete pet by invalid Id")
								 public void theRequestIsMadeWithTheDeleteMethodToDeletePetByInvalidId() {
								SerenityRest.when().post(PetEndpoints.DELETEPET.getUrl());
												
											    }

								 @Then("the pet to be deleted not found is returned")
								  public void thePetToBeDeletedNotFoundIsReturned() {
								       restAssuredThat(response -> response
									                .statusCode(HttpStatus.SC_NOT_FOUND).body("message",equalTo("not found")));
											        
											    }
}
