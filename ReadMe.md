## Description
This is a skeleton framework to show how the PetStore API (https://petstore.swagger.io) can be tested using Serenity and Cucumber.

## Setup
### Run Locally
The project is hosted on GitLab. It can be either downloaded as a zip file or cloned.

#### To download locally as zip file follow these steps
> * open in a browser the link https://gitlab.com/arpanab6/cucumberserenitytest</br>
> * select master branch
> * click the dropdown and select zip
>
#### To download locally by cloning the repository
> * open Terminal (on MacOS or Linux) or CommnandLine (on Windows) and run 
> ```git --version```
 to check if git is already installed on the machine. If the version of git is not displayed then do the next step.
> * (OPTIONAL) Download and install Git following the instructions from https://git-scm.com/download/ 
> * Run ```git clone https://gitlab.com/arpanab6/cucumberserenitytest.git```

#### To run the tests locally run
> * ```cd cucumberserenitytest```
> * ```mvn clean verify```

#### Check report
After the tests are run a report is generated. The report link is displayed at the end of the log. It should be something like:
file://path/to/cucumberSerenitytest/target/site/serenity/index.html. The link can be opened in a browser. 

The report will contain information about:
> * Features that were tested
> * Scenarios for every Feature
> * Test Result for each Scenario
> * the logged request and response, with all details

### Run in CI
The CI pipeline was created in GitLab. On each commit the pipeline is run. The pipeline has 2 phases
> * Build - that build the code
> * Test - that runs the tests and saves the Test Report as an artifact 

The overview of all the runs is available here https://gitlab.com/arpanab6/cucumberserenitytest/-/pipelines
For each run the Test Report is saved and stored for 30 days. The report can be downloaded locally or browsed directly in GitLab.

## Issues Found
* application returns StatusCode 200 even when it should not. Because of this most of the positive and negative test were failing the StatusCode check.

## Framework structure

> * Runner class (AcceptanceTestSuite)

The Test Runner helps running the feature files and it connects feature files with the Step Definition classes

> * Models

Contain classes that that help with creating and parsing data.

> * Step Definition files

Classes that hold the code that automates the steps from the Feature files.
> * Features files

Plain language files, that use Gherkin syntax, to define the expected behaviour of the application.

> * EndPoints class

Helper class to hold the API endpoints that we want to test.

## How to add new tests
> * Add a new Feature file/or add new Scenarios to existing feature file depending on the functionality that needs to be tested.
> * Keep Feature and StepDefinition files manageable in size.
> * Extract common functionality in Step Libraries.
> * Write new test Scenarios in Gherkin format.
> * Automate the scenario steps using Java.
> * Test first locally to check that everything is fine and then commit.
> * Add new endpoints to ```Endpoints class```

