
 Feature: Everything about your pets

	Scenario: An image upload for existing pet
 		Given the request with pet Id 1 and valid image
 		When the request is made with the post method to upload image 
		Then the image uploaded successfully is returned
		

 	Scenario: Add new pet details to the store
 		Given the request with pet details
 		When the request is made with the post method to add new pets
		Then the pet added successfully is returned
		
	Scenario: update a pet details to the store
 		Given the request with pet details to be updated
 		When the request is made with the put method to update or create new pets
		Then the pet updated or added successfully is returned
		
	Scenario: find pet details by status
 		Given the request with "available" status
 		When the request is made with the get method to find pet by status
		Then the pet details successfully is returned
		
	Scenario: find pet by Id
 		Given the request with Id 1
 		When the request is made with the get method to find pet by Id
		Then the pet details successfully is returned by Id
		
	Scenario: find pet by invalid Id
 		Given the request with invalid Id 1345122
 		When the request is made with the get method to find pet by invalid Id
		Then the Pet not found is returned
		
	Scenario: update pet name and status by Id
 		Given the request with  Id 1 and status "pending" and name "Golden"
 		When the request is made with the post method to update pet by Id
		Then the pet details successfully updated is returned
		
	Scenario: update pet name and status by invalid Id
 		Given the request with invalid Id 123344 and status "pending" and name "Golden"
 		When the request is made with the post method to update pet by invalid Id
		Then the pet details not found is returned
		
	Scenario: delete pet by Id
 		Given the request to delete pet with  Id 1
 		When the request is made with the delete method to delete pet by Id
		Then the pet details successfully deleted is returned
		
	Scenario: delete pet by invalid Id
 		Given the request to delete pet with invalid Id 12345
 		When the request is made with the delete method to delete pet by invalid Id
		Then the pet to be deleted not found is returned