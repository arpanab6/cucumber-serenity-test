
 Feature: Access to Pet store orders

	Scenario: place an order for a pet
 		Given the request with order details
 		When the request is made with the post method to place order 
		Then the order placed successfully is returned
		
	Scenario: place an invalid order for a pet
 		Given the request with invalid order details
 		When the request is made with the post method to place invalid order 
		Then the invalid order placed successfully is returned
		
	Scenario: get an order for a pet
 		Given the request with order Id 
 		When the request is made with the Get method to retrieve an order 
		Then the order get successfully is returned
		
	Scenario: get an order not exist for a pet
 		Given the request with order Id not exist
 		When the request is made with the Get method to retrieve an order not exist
		Then the order not found is returned
		
	Scenario: retrieve store inventory
 		Given the request to retrieve inventory  
 		When the request is made with the Get method to retrieve inventory 
		Then the inventory get successfully is returned
	
	Scenario: delete an order by orderId
 		Given the request to delete an order with Id 
 		When the request is made with the delete method
		Then the order deleted successfully is returned
		
	Scenario: delete an order does not exist by Id
 		Given the request to delete an order with Id not exist 
 		When the request is made with the delete method for order not exist
		Then the order does not exist is returned